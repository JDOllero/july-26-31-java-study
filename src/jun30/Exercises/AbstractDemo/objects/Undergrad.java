package jun30.Exercises.AbstractDemo.objects;

public class Undergrad extends Student
{
	public Undergrad (String name)
	{
		super(name);
	}
	
	@Override
	public void generateResult()
	{
		int total=0;
		int average=0;
		for(int score: this.getTest())
		{
			total+=score;
		}
		average = total/this.getTest().length;
		if(average >=60)
		{
			this.setResult("Pass");
		}
		else
		{
			this.setResult("Fail");
		}
		System.out.println("Name: "+this.getName());
		System.out.println("Result: "+this.getResult());
	}

}
