package jun30.Exercises.AbstractDemo.executable;

import jun30.Exercises.AbstractDemo.objects.Grad;
import jun30.Exercises.AbstractDemo.objects.Undergrad;

public class Tester 
{
	
	public static void main(String[] args)
	{

		Grad student1 = new Grad("Ajay");
		Undergrad student2 = new Undergrad("Mehul");
		student1.setTestScore(0,70);
		student1.setTestScore(1,69);
		student1.setTestScore(2,71);
		student1.setTestScore(3,55);
	
		student2.setTestScore(0,70);
		student2.setTestScore(1,69);
		student2.setTestScore(2,71);
		student2.setTestScore(3,55);
		
		student1.generateResult();
		student2.generateResult();
	}
}
