package jun30.Exercises.AssociationExercise.objects;

public class InterestCalculator 
{
	private int timeInYears;
	private float rateOfInterest;
	
	public InterestCalculator(int timeInYears, float rateOfInterest) 
	{
		super();
		this.timeInYears = timeInYears;
		this.rateOfInterest = rateOfInterest;
	}
	
	public float calculateInterest(float principal)
	{
		return ((timeInYears*rateOfInterest*principal)/100);
	}
	

	public int getTimeInYears() 
	{
		return timeInYears;
	}

	public void setTimeInYears(int timeInYears) 
	{
		this.timeInYears = timeInYears;
	}

	public float getRateOfInterest() 
	{
		return rateOfInterest;
	}

	public void setRateOfInterest(float rateOfInterest) 
	{
		this.rateOfInterest = rateOfInterest;
	}
	
	
}
