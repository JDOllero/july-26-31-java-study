package jun30.Exercises.AssociationExercise.objects;

public class FDAccount 
{
	private int accountId;
	private float balance;


	public FDAccount(int accountId, float balance) 
	{
		super();
		this.accountId = accountId;
		this.balance = balance;
	}

	public void updateBalance(InterestCalculator interestCalculator)
	{
		balance+=(interestCalculator.calculateInterest(balance));
		System.out.println("Your FD Account has been credited for its interest");
		System.out.println("Your current balance is: Rs."+balance);
	}


	public int getAccountId() 
	{
		return accountId;
	}


	public void setAccountId(int accountId) 
	{
		this.accountId = accountId;
	}


	public float getBalance() 
	{
		return balance;
	}


	public void setBalance(float balance) 
	{
		this.balance = balance;
	}


}
