package jun30.Exercises.AssociationExercise.executable;

import jun30.Exercises.AssociationExercise.objects.FDAccount;
import jun30.Exercises.AssociationExercise.objects.InterestCalculator;

public class Tester {

	public static void main(String[]args)
	{
		InterestCalculator interestCalculator = new InterestCalculator(1, 8.4f);
		FDAccount fdAccount = new FDAccount(1010, 50000);
		fdAccount.updateBalance(interestCalculator);
	}
	
}
