package jun30.Exercises.SoudagarTravels.objects;

public class SeniorTraveler extends Traveler
{
	private int age;

	public SeniorTraveler(String name, String id, int age) {
		super(name, id);
		this.age = age;
	}
	
	@Override
	public void calculateFare()
	{
		if (age > 65)
		{
			this.setFare(this.getFare()*0.85*1.1136);
		}
		else if (age > 50)
		{
			this.setFare(this.getFare()*0.9*1.1136);
		}
		else
		{
			this.setFare(-1);
		}
	}
	
	@Override
	public void displayDetails()
	{
		calculateFare();
		if(this.getFare() ==-1)
		{
		System.out.println("Apologies, "+this.getName()+". You are not old enough to receive senior discounts");
		System.out.println();
		}
		else
		{
		System.out.println("Traveler Details");
		System.out.println("****************");
		System.out.println("Name: "+this.getName());
		System.out.println("Id: "+this.getId());
		System.out.println("Cost: "+this.getFare());
		System.out.println();
		}
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	

}
