package jun30.Exercises.SoudagarTravels.objects;

public class Traveler 
{
	private String name;
	private String id;
	private double fare = 2000;

	public Traveler(String name, String id) 
	{
		super();
		this.name = name;
		this.id = id;
	}
	
	public void calculateFare()
	{
		fare*=1.1136;
	}
	
	public void displayDetails()
	{
		calculateFare();
		System.out.println("Traveler Details");
		System.out.println("****************");
		System.out.println("Name: "+name);
		System.out.println("Id: "+id);
		System.out.println("Cost: "+fare);
		System.out.println();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getFare() {
		return fare;
	}

	public void setFare(double fare) {
		this.fare = fare;
	}

	
}
