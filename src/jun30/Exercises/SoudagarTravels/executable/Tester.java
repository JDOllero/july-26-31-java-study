package jun30.Exercises.SoudagarTravels.executable;

import jun30.Exercises.SoudagarTravels.objects.SeniorTraveler;
import jun30.Exercises.SoudagarTravels.objects.Traveler;

public class Tester 
{

	public static void main(String[]args)
	{
	Traveler traveler1 = new Traveler("Priya", "AQW1344321");
	Traveler senior1 = new SeniorTraveler("Joey", "PJAMG7755TY", 70);
	Traveler senior2 = new SeniorTraveler("Raj", "87657683546", 48);
	
	traveler1.displayDetails();
	senior1.displayDetails();
	senior2.displayDetails();
	}
}
