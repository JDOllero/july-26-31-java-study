package jun30.Assignment.Assignment3.executable;

import jun30.Assignment.Assignment3.objects.EPayWallet;
import jun30.Assignment.Assignment3.objects.KYCUser;
import jun30.Assignment.Assignment3.objects.User;

public class Tester 
{
	public static void main(String[] args)
	{
	
		User user1 = new User(101, "Jack", "Jack@infy.com", 1000);
		User user2 = new KYCUser(201, "Jill", "Jill@infy.com", 3000, 0);
		
		EPayWallet ePayW = new EPayWallet();
		
		ePayW.processPaymentByUser(user1, 700);
		ePayW.processPaymentByUser(user2, 1500);
		ePayW.processPaymentByUser(user2, 800);
		ePayW.processPaymentByUser(user2, 1200);
	}
}
