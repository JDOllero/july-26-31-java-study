package jun30.Assignment.Assignment3.objects;

public class KYCUser extends User
{
	private int rewardPoints;

	public KYCUser(int id, String username, String email, double walletBalance, int rewardPoints) {
		super(id, username, email, walletBalance);
		this.rewardPoints = rewardPoints;
	}

	@Override
	public boolean makePayment(double billAmount)
	{
		if(this.getWalletBalance() < billAmount)
		{
		return false;
		}
		else
		{
			this.setWalletBalance(this.getWalletBalance()-billAmount);
			rewardPoints+=billAmount/10;
			return true;
		}
		
	}
	
	public int getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
}
