package jun30.Assignment.Assignment3.objects;

public class EPayWallet {

	public void processPaymentByUser(User user, double billAmount)
	{
		if(user.makePayment(billAmount))
		{
			System.out.println(user.getUsername()+", your payment of "+billAmount+" was successful.");
		}
		else
		{
			System.out.println(user.getUsername()+", you lack the funds to make your payment.");
		}
		System.out.println("Your current balance is "+user.getWalletBalance());
		if(user instanceof KYCUser)
		{
			System.out.println("You have "+((KYCUser) user).getRewardPoints()+" reward points.");
		}
		System.out.println("--------------------------------------");
	}
}
