package jun31.Exercises.InterfaceExercise.Executable;

import jun31.Exercises.InterfaceExercise.Objects.Intern;
import jun31.Exercises.InterfaceExercise.Objects.Trainee;

public class Tester {
	public static void main(String[] args){
	Trainee trainee = new Trainee(370);
	Intern intern = new Intern(283, 77);
	trainee.calcPercentage();
	intern.calcPercentage();
	}
}
