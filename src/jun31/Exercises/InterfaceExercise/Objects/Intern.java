package jun31.Exercises.InterfaceExercise.Objects;

import jun31.Exercises.InterfaceExercise.Interfaces.Student;

public class Intern implements Student{
	int marksSecured;
	int projectMarks;
	int total;
	
	public Intern(int marksSecured, int projectMarks) 
	{
		super();
		this.marksSecured = marksSecured;
		this.projectMarks = projectMarks;
	}


	@Override
	public void calcPercentage() 
	{
		if (marksSecured+projectMarks > TOTAL_MAXIMUM_MARKS){
			System.out.println("Total Marks Recorded over eligible maximum.");
		}
		else System.out.println("Your aggregate percentage as an Intern is "+((double)(marksSecured+projectMarks)*100/TOTAL_MAXIMUM_MARKS));
	}
	
}
