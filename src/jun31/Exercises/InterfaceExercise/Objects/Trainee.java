package jun31.Exercises.InterfaceExercise.Objects;

import jun31.Exercises.InterfaceExercise.Interfaces.Student;

public class Trainee implements Student{
	int marksSecured;
	
	public Trainee(int marksSecured) 
	{
		super();
		this.marksSecured = marksSecured;
	}

	@Override
	public void calcPercentage() 
	{
		if (marksSecured > TOTAL_MAXIMUM_MARKS){
			System.out.println("Total Marks Recorded over eligible maximum.");
		}
		else System.out.println("Your aggregate percentage as a Trainee is "+(100*(double)marksSecured/TOTAL_MAXIMUM_MARKS));
	}

}
