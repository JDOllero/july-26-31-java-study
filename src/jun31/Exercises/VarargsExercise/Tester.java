package jun31.Exercises.VarargsExercise;

import jun31.Exercises.VarargsExercise.Services.MyMath;
//returns only 1 decimal for the deviation instead of the 10 the exercise asks for
public class Tester {
	public static void main(String[] args){
		int[] x = {2, 4, 5, 7, 6};
		System.out.println("Mean is " + MyMath.mean(x));
		System.out.println("Standard Deviation is "+MyMath.deviation(x));
		}
}
