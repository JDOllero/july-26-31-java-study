package jun31.Exercises.VarargsExercise.Services;

public class MyMath {
	public static double deviation(int... x){
		double sum = 0.0;
		double average =0.0;
		for(int i: x){
			sum += Math.abs((i-mean(x)));
		}
		average= sum/x.length;
		return Math.sqrt(average);
	}

	public static double mean(int... x){
		double sum=0.0;
		for (int i:x){
			sum+=i;
		}
		return (sum/x.length);
	}
}
