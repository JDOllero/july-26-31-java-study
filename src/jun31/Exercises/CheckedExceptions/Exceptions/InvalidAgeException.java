package jun31.Exercises.CheckedExceptions.Exceptions;

public class InvalidAgeException extends Exception{

	public InvalidAgeException(){
		super("Invalid age.");
	}
}
