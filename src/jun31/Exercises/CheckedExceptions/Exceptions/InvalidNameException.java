package jun31.Exercises.CheckedExceptions.Exceptions;

public class InvalidNameException extends Exception{
	public InvalidNameException(){
		super("Invalid applicant name.");
	}

}
