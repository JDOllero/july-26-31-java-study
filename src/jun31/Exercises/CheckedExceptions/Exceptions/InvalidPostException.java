package jun31.Exercises.CheckedExceptions.Exceptions;

public class InvalidPostException extends Exception{
	public InvalidPostException(){
		super("Invalid post.");
	}
}
