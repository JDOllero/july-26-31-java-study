package jun31.Exercises.CheckedExceptions.Services;

import javax.naming.InvalidNameException;

import jun31.Exercises.CheckedExceptions.Exceptions.InvalidAgeException;
import jun31.Exercises.CheckedExceptions.Exceptions.InvalidPostException;
import jun31.Exercises.CheckedExceptions.Objects.Applicant;

public class Validator {
	
	public void validate(Applicant applicant) throws InvalidNameException, InvalidPostException, InvalidAgeException{
		try{
			if(!isValidName(applicant.getApplicantName())){
				throw new InvalidNameException();
			}
			if(!isValidPost(applicant.getPostApplied())){
				throw new InvalidPostException();
			}
			if(!isValidAge(applicant.getApplicantAge())){
				throw new InvalidAgeException();
			}
			System.out.println("All values are valid");
		}catch(InvalidNameException ine){
			ine.printStackTrace();
		}catch(InvalidPostException ipe){
			ipe.printStackTrace();
		}catch(InvalidAgeException iae){
			iae.printStackTrace();
		}finally{
			System.out.println();
		}
	}

	public boolean isValidName(String name){
		if(name == null){
		return false;
		}
		else if(name ==""){
			return false;
		}
		else return true;
	}
	
	public boolean isValidPost(String post){
		if(!(post=="Probationary Officer"||post=="Assistant"||post=="Special Cadre Officer")) return false;
		else return true;
	}
	
	public boolean isValidAge(int age){
		if(age > 18 && age < 35) return true;
		else return false;
	}
	
}
