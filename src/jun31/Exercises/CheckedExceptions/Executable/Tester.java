package jun31.Exercises.CheckedExceptions.Executable;

import javax.naming.InvalidNameException;

import jun31.Exercises.CheckedExceptions.Exceptions.InvalidAgeException;
import jun31.Exercises.CheckedExceptions.Exceptions.InvalidPostException;
import jun31.Exercises.CheckedExceptions.Objects.Applicant;
import jun31.Exercises.CheckedExceptions.Services.Validator;

public class Tester {
	public static void main(String[]args) throws InvalidNameException, InvalidPostException, InvalidAgeException{
		Applicant applicant1 = new Applicant();
		Applicant applicant2 = new Applicant();
		Applicant applicant3 = new Applicant();
		Applicant applicant4 = new Applicant();
		Applicant applicant5 = new Applicant();
		Validator validator = new Validator();
		
		applicant1.setApplicantName("Jennifer");
		applicant2.setApplicantName("");
		applicant4.setApplicantName("Brittany");
		applicant5.setApplicantName("Deandra");
		
		applicant5.setPostApplied("Special Cadre Officer");
		applicant4.setPostApplied("The Most Popular Girl in School");
		applicant1.setPostApplied("Probationary Officer");
		
		applicant1.setApplicantAge(55);
		applicant5.setApplicantAge(24);
		
		validator.validate(applicant1);
		validator.validate(applicant2);
		validator.validate(applicant3);
		validator.validate(applicant4);
		validator.validate(applicant5);
	}
	
}
