package jun31.CodeSnippets.ExceptionDemo;
//A Demo showing off the order of execution of lines of code when faced with an exception.
public class Tester {    
	public static void main(String arg[]) {
    int x=5, y=0;
    System.out.println("Starting division...");
    
    try {
        if(y==0) throw new Exception("Divisor cannot be zero");
        int z = x/y;
        System.out.println("z = " + z);
    }
    catch(Exception e) {
        System.out.println(e.getMessage());
    }
    finally {
        System.out.println("finally is executed anyway");
    }
    
    System.out.println("Execution continues after try-catch-finally");
}

}
