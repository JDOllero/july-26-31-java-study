package jun31.Assignments.CourseCertification.Implementation;

public class CrashCourseCertification extends RRTechnicalCertification{

	private final int courseDuration = 2;

	public CrashCourseCertification(String studentName, String courseName, int admissionTestMarks) {
		super(studentName, courseName, admissionTestMarks);
		generateRegistrationId();// TODO Auto-generated constructor stub
	}

	@Override
	public void generateRegistrationId() {
		if(RRTechnicalCertification.counter%2 == 1) this.setRegistrationId(this.getRegistrationId()+RRTechnicalCertification.counter+1);
		else this.setRegistrationId(this.getRegistrationId()+RRTechnicalCertification.counter);
		RRTechnicalCertification.counter++;
	}

	@Override
	public double calculateFee() {	
	if (this.getAdmissionTestMarks() >=90){
		return CRASH_COURSE_FEE*0.9*1.1236;
	}
	else if(this.getAdmissionTestMarks() >= 75){
		return CRASH_COURSE_FEE*0.95*1.1236;
	}
	else return CRASH_COURSE_FEE*1.1236;
	}
	
	@Override
	public void displayDetails() {
	System.out.println("Crash Course Registration Details");
	System.out.println("************************************");
	System.out.println("Student Name\t: "+this.getStudentName());
	System.out.println("Course Name\t: "+this.getCourseName());
	System.out.println("Course Duration\t: "+courseDuration+ " Months");
	System.out.println("Registration ID\t: "+this.getRegistrationId());
	System.out.println("Fees \t: " + calculateFee());
	System.out.println("");
	}
	
	public int getCourseDuration() {
		return courseDuration;
	}



}
