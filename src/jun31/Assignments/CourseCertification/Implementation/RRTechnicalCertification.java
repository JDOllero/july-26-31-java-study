package jun31.Assignments.CourseCertification.Implementation;

import jun31.Assignments.CourseCertification.Interfaces.Certification;

public abstract class RRTechnicalCertification implements Certification{
	private String studentName;
	private String courseName;
	private int registrationId = 1000;
	private int admissionTestMarks;
	public static int counter=0;
	
	
	public RRTechnicalCertification(String studentName, String courseName, int admissionTestMarks) {
		super();
		this.studentName = studentName;
		this.courseName = courseName;
		this.admissionTestMarks = admissionTestMarks;
	}
	
	public abstract void generateRegistrationId();
	
	public abstract double calculateFee();
	
	public abstract void displayDetails();

	public String getStudentName() {
		return studentName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public int getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(int registrationId) {
		this.registrationId = registrationId;
	}

	public int getAdmissionTestMarks() {
		return admissionTestMarks;
	}

	public void setAdmissionTestMarks(int admissionTestMarks) {
		this.admissionTestMarks = admissionTestMarks;
	}
	

}
