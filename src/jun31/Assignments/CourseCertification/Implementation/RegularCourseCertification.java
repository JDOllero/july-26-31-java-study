package jun31.Assignments.CourseCertification.Implementation;

public class RegularCourseCertification extends RRTechnicalCertification{
private int courseDuration;

public RegularCourseCertification(String studentName, String courseName, int admissionTestMarks, int courseDuration) {
	super(studentName, courseName, admissionTestMarks);
	this.courseDuration = courseDuration;
	generateRegistrationId();
}

@Override
public void generateRegistrationId() {
	if(RRTechnicalCertification.counter%2 ==0) this.setRegistrationId(this.getRegistrationId()+RRTechnicalCertification.counter+1);
	else this.setRegistrationId(this.getRegistrationId()+RRTechnicalCertification.counter);
	RRTechnicalCertification.counter++;
}

@Override
public double calculateFee() {
	// TODO Auto-generated method stub
	if (this.getAdmissionTestMarks() >=90){
		return REGULAR_COURSE_FEE*0.9*courseDuration;
	}
	else if(this.getAdmissionTestMarks() >= 75){
		return REGULAR_COURSE_FEE*0.95*courseDuration;
	}
	else return REGULAR_COURSE_FEE*courseDuration;
}

public void displayDetails(){
	System.out.println("Regular Course Registration Details");
	System.out.println("************************************");
	System.out.println("Student Name\t: "+this.getStudentName());
	System.out.println("Course Name\t: "+this.getCourseName());
	System.out.println("Course Duration\t: "+courseDuration + " Months");
	System.out.println("Registration ID\t: "+this.getRegistrationId());
	System.out.println("Fees \t: " + calculateFee());
	System.out.println();
}

public int getCourseDuration() {
	return courseDuration;
}

public void setCourseDuration(int courseDuration) {
	this.courseDuration = courseDuration;
}



}
