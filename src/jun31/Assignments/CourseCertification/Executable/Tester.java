package jun31.Assignments.CourseCertification.Executable;

import jun31.Assignments.CourseCertification.Implementation.CrashCourseCertification;
import jun31.Assignments.CourseCertification.Implementation.RegularCourseCertification;

public class Tester {
	public static void main(String[] args){
		RegularCourseCertification cert2 = new RegularCourseCertification("Rakesh", "J2EE", 85, 5);
		CrashCourseCertification cert1 = new CrashCourseCertification("Roshan", "Angular", 71);
		
		cert2.displayDetails();
		cert1.displayDetails();
	}

}
