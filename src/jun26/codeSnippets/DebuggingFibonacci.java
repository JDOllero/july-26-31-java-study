package jun26.codeSnippets;
//Compiled but returned unexpected values (Expected 0 0 1 2 3 5; Got 0 0 0 0 0 0)
public class DebuggingFibonacci {
	public static void main(String[] args) {
		int fiboCount = 20;
		int no1 = -1, no2 = 1, no3;
		System.out.println("Fibonacci series:");
		for (int i = 0; i < fiboCount; i++) {
			no3 = no1 + no2;
			System.out.print(no3 + " ");
			//no2 = no3;
			//no1 = no2;
			//The order of assigning new values was backwards
			//By assigning no2 to no3 before assigning no2 to no1, you turned all variables to 0
			//Resulting in 0 0 0 0 0 0 instead of 0 1 1 2 3 5....
			no1 = no2;
			no2 = no3;
		}
	}
}
