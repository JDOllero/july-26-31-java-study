package jun26.codeSnippets;

//Compiled, but returned unintended values (FIXED)
//Expected Output:
//Product Id: 101
//Product Name: "Jeans"
//
//Output:
//Product Id: 0
//Product Name: null
public class Product 
{
	private int productId;
	private String name;
	
	public Product(int productId, String name) 
	{
		//FIX
		//Include this.x to your variables to qualify the variable assigned to the object
		//being created.
		this.productId = productId;
		this.name = name;
	}
	
	public static void main(String[] args) 
	{
		Product p1 = new Product(101, "Jeans");
		System.out.println("Product Id: "+ p1.productId);
		System.out.println("Product Name: "+ p1.name);
	}
}
