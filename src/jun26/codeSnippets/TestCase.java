package jun26.codeSnippets;
//A working set of code, saved for demonstration purposes
//A static block is executed when its class is called
//Output:
//Static block in Tester class
//In main
//Static block in Printer class
//The value of sample variable is: 2
class Printer {
    static {
    	
        System.out.println("Static block in Printer class");
    }
    
    public static void display(String message) {
        System.out.println(message);
    }
}

public class TestCase{
    public static int sampleVariable = 1;

    static {
    	//Happens first because you need to call this class to execute the main method
        System.out.println("Static block in Tester class");
    }
    
    public static void main(String[] args) {
        sampleVariable++;
        Printer p = null;
        //Since you don't call on a constructor, the declaration of p above 
        //doesn't call on Printer's Static block
        System.out.println("In main");
        //Since you need to call on the printer class to use the method in the line below
        //The static block happens before the method is called.
        //Calling on the null p object is unnecessary
        p.display("The value of sample variable is: " + sampleVariable);
    }
}