package jun26.codeSnippets;
//Fixed: Compilation error on lines 25-27 
public class Tester {
	public static void main(String[] args) {
		Car c1 = new Car("Red");
		Car c2 = new Car("Green");
		Car c3 = new Car("Blue");
		System.out.println("Number of cars created: " + Car.getNumberOfCars());
	}
	
	static {
		System.out.println("Tester class loaded");
	}
}

class Car {
    private String color;
	private static int numberOfCars = 0;
	
	public Car(String color) {
	    this.color = color;
		numberOfCars++;
	}
	//Do not use a static method to return an instance value.
	public /*static*/ String getColor() {
	    return this.color;
	}
	
	public static int getNumberOfCars() {
		return numberOfCars;
	}

	static {
		System.out.println("Car class loaded");
	}
}
