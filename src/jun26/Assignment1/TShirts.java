package jun26.Assignment1;
//This program calculates price of an array of shirts and displays a subtotal
//The subtotal may be discounted if the number of shirts is high enough
public class TShirts {

	public static void main(String[] args)
	{
		double subtotal = 0.0; 
		String[] cart = {"round-neck", "collared", "hooded", "round-neck", "round-neck"};
		for(String shirts:cart)
		{
		if(shirts == "round-neck")
			{
			subtotal+=200;
			}
		else if (shirts =="collared")
			{
			subtotal+=250;
			}
		else if (shirts =="hooded")
			{
			subtotal+=300;
			}
		}
		if (cart.length >= 5)
		{
		if (cart.length > 10)
			{
			subtotal *= 0.8;
			}
		else subtotal *= 0.9;
		}
		System.out.println("Your total is Rs. "+subtotal);
	}
}
