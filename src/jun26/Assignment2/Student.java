package jun26.Assignment2;
//Implementation of the methods seen in CourseRegistration
//Features include: 
//-Collection of a Student's information, including Name, Id, Exam Marks, and Hostel requirement
//-Calculation of a Course's fee based on the student's Exam marks and the course's ID
//-Determining a Student's qualification based on Exam Marks
//-Checking for a valid Course ID
public class Student 
{
//Instance Variables
private String studentName;
private int registrationId;
private float qualifyingMarks;
private double courseFee;
private int courseId;
private boolean hostelRequired;

//Constructors
public Student(){}

public Student(String studentName, int registrationId, float qualifyingMarks, int courseId,
	boolean hostelRequired) 
	{
		super();
		this.studentName = studentName;
		this.registrationId = registrationId;
		this.qualifyingMarks = qualifyingMarks;
		this.courseFee = 0;
		this.courseId = courseId;
		this.hostelRequired = hostelRequired;
	}

//special Methods
public boolean validateMarks()
	{
	
		if(this.getQualifyingMarks() >= 65 && this.getQualifyingMarks() <= 100)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

@Override
public String toString() 
	{
		if (!this.validateMarks())
		{
			return "Marks is less than 65.  You are not eligible for admission";
		}
		else if(!this.validateCourseId())
		{
			return "Invalid Course Id.  Please Try Again.";
		}
		else
			{
				this.calculateCourseFee();
				return "COURSE ALLOCATION DETAILS \n Student Name :" + studentName + "\n Student's Registration Id :" 
						+ registrationId + "\n Qualifying Exam Marks :"
						+ qualifyingMarks + "\n Total Course Fee :" + courseFee + "\n Course Id :" + 
						courseId + "\n Hostel Required :" + hostelRequired;
			}
	}

public boolean validateCourseId()
	{
	if(this.getCourseId() >= 1001 && this.getCourseId() <=1005)
		{
			return true;
		}
	else
		{
			return false;
		}	
	}

public void calculateCourseFee()
	{
		if(validateCourseId())
		{
		switch(this.getCourseId())
			{
				case 1001: this.setCourseFee(55_000.0); break;
				case 1002: this.setCourseFee(35_675.0); break;
				case 1003: this.setCourseFee(28_300.0); break;
				case 1004: this.setCourseFee(22_350.0); break;
				case 1005: this.setCourseFee(1_15_000.0); break;
			}
			if(this.getQualifyingMarks() >=85)
			{
				this.setCourseFee(this.getCourseFee()*0.85);
			}
			else if(this.getQualifyingMarks() >= 70)
			{
				this.setCourseFee(this.getCourseFee()*0.9);
			}
			else
			{
				this.setCourseFee(this.getCourseFee()*0.95);
			}
		}
	}

//Getters+Setters
public String getStudentName() 
	{
		return studentName;
	}
public void setStudentName(String studentName) 
	{
		this.studentName = studentName;
	}
public int getRegistrationId() 
	{
		return registrationId;
	}
public void setRegistrationId(int registrationId) 
	{
		this.registrationId = registrationId;
	}
public float getQualifyingMarks() 
	{
		return qualifyingMarks;
	}
public void setQualifyingMarks(float qualifyingMarks) 
	{
		this.qualifyingMarks = qualifyingMarks;
	}
public double getCourseFee() 
	{
		return courseFee;
	}
public void setCourseFee(double courseFee) 
	{
		this.courseFee = courseFee;
	}
public int getCourseId() 
	{
		return courseId;
	}
public void setCourseId(int courseId) 
	{
		this.courseId = courseId;
	}
public boolean isHostelRequired() 
	{
		return hostelRequired;
	}
public void setHostelRequired(boolean hostelRequired) 
	{
		this.hostelRequired = hostelRequired;
	}



}
