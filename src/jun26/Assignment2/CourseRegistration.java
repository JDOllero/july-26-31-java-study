package jun26.Assignment2;
//An application to register a student to a university.
//Methods to be handled in Student.java
public class CourseRegistration 
{
	public static void main (String[] args)
	{
		Student s1 = new Student("Peter", 5001, 58, 1005, true),
				s2 = new Student("Peter", 5001, 68, 1006, true),
				s3 = new Student("Peter", 5001, 78, 1005, false);
		System.out.println(s1.toString());
		System.out.println(s2.toString());
		System.out.println(s3.toString());
	}
}
