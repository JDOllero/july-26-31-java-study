package jun29.Exercises.aggregationExercise.executable;

import jun29.Exercises.aggregationExercise.objects.Address;
import jun29.Exercises.aggregationExercise.objects.RRAccount;

//Jul 29: Created RRAccount+Address Classes, added fields, getters, and a constructor to Address
public class Tester {
	public static void main(String[] args)
	{
		Address address1 = new Address("No.123", "9th Cross, Trinethra Circle", "Mysuru", "Karnataka", 70017);
		Address address2 = new Address("No.123", "9th Cross, Trinethra Circle", "Mysuru", "Karnataka", 570017);
		RRAccount rrac1 = new RRAccount("Rakesh", address1, 50000.00);
		RRAccount rrac2 = new RRAccount("Rakesh", address2, 50000.00);
		rrac1.output();
		rrac2.output();
	}
}
