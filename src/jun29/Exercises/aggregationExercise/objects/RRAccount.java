package jun29.Exercises.aggregationExercise.objects;

public class RRAccount {
private String customerName;
private Address address;
private double depositAmount;

//Constructor
public RRAccount(String customerName, Address address, double depositAmount) {
	super();
	this.customerName = customerName;
	this.address = address;
	this.depositAmount = depositAmount;
}

public boolean isValidAddress()
{
	if (String.valueOf(address.getPinCode()).length()==6) return true;
	else return false;
}

public void output()
{
	if(!isValidAddress())
	{
	System.out.println("This is an invalid pin"); 	
	}
	else
	{
		System.out.println("Customer Details");
		System.out.println("******************************");
		System.out.println("Customer Name\t :"+customerName);
		System.out.println("Address\t :"+address.getFirstLine()+" "+address.getSecondLine()+" "+address.getCity()+ " "+address.getState() + " "+address.getPinCode());
		System.out.println("Deposit Amount\t :" + depositAmount);
		System.out.println("Congratulations!  You have created an account");
	}
}

//Getters
public String getCustomerName() {
	return customerName;
}

public Address getAddress() {
	return address;
}

public double getDepositAmount() {
	return depositAmount;
}


}
