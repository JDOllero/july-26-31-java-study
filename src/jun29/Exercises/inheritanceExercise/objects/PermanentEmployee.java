package jun29.Exercises.inheritanceExercise.objects;

public class PermanentEmployee extends Employee{
	private double basicPay;
	private double hra;
	private int experience;
	
	//Constructor
	public PermanentEmployee(int empId, String name, double basicPay, double hra, int experience) {
		super();
		this.setEmpId(empId);
		this.setName(name);
		this.basicPay = basicPay;
		this.hra = hra;
		this.experience = experience;
	}
	
	
	//Other Methods

	public void calculateSalary()
	{
		double multiplier;
		if (this.getExperience() >= 10)
		{
			multiplier=1.12;
		}
		else if (this.getExperience() >=5)
		{
			multiplier=1.07;
		}
		else if (this.getExperience() >=3)
		{
			multiplier = 1.05;
		}
		else
		{
			multiplier = 1.0;
		}
		this.setSalary(multiplier*basicPay+hra);
		System.out.println("Permanent Employee: Your salary is: "+this.getSalary());
	}
	
	//Getters and Setters
	public double getBasicPay() {
		return basicPay;
	}
	public void setBasicPay(double basicPay) {
		this.basicPay = basicPay;
	}
	public double getHra() {
		return hra;
	}
	public void setHra(double hra) {
		this.hra = hra;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	
	

}
