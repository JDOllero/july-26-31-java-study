package jun29.Exercises.inheritanceExercise.objects;

public class ContractEmployee extends Employee{
	private double wage;
	private int hours;
	
	//constructor
	public ContractEmployee(int empId, String name, double wage, int hours) 
	{
		super();
		this.setEmpId(empId);
		this.setName(name);
		this.wage = wage;
		this.hours = hours;
	}

	//other methods
	public void calculateSalary()
	{
		this.setSalary(hours*wage);
		System.out.println("Contract Employee: Your salary is: "+this.getSalary());
	}
	
	//setters and getters
	public double getWage() 
	{
		return wage;
	}

	public void setWage(double wage) 
	{
		this.wage = wage;
	}

	public int getHours() 
	{
		return hours;
	}

	public void setHours(int hours) 
	{
		this.hours = hours;
	}
	
	
}
