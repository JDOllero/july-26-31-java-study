package jun29.Exercises.inheritanceExercise.objects;

//The base Employee Class, to be extended by two other Classes
//PermanentEmployee and ContractEmployee

public class Employee {
	//These variables represent values that employees of all types have
	//The first 2 are values that go into creation of Employee Objects of both kinds
private int empId;
private String name;
//This value is calculated differently based on the type of employee and factors specific to the employee type.
//For Reference: Contract Employees are a direct multiplier based on hours and wages
//Permanent Employees, meanwhile, have their pay based on a formula based on their years of experience
private double salary;

//Getters And Setters
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getSalary() {
	return salary;
}
public void setSalary(double salary) {
	this.salary = salary;
}
}
