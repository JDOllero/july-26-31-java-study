package jun29.Exercises.inheritanceExercise.Executable;

//This exercise is to demonstrate Inheritance using two 
//Child Classes of Employee: PermanentEmployee and ContractEmployee
import jun29.Exercises.inheritanceExercise.objects.ContractEmployee;
import jun29.Exercises.inheritanceExercise.objects.PermanentEmployee;

public class Tester {
public static void main(String[] args)
	{
		PermanentEmployee employee1 = new PermanentEmployee(101, "Anil", 10000, 1500, 3);
		ContractEmployee employee2 = new ContractEmployee(102, "Ankit", 500, 10);
		
		employee1.calculateSalary();
		employee2.calculateSalary();

	}
}
